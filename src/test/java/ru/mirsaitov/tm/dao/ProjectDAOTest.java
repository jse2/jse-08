package ru.mirsaitov.tm.dao;

import org.junit.Test;
import ru.mirsaitov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ProjectDAOTest {

    private final String name = "name";

    private final String description = "description";

    private final ProjectDAO projectDAO = new ProjectDAO();

    @Test
    public void shouldCreate() {
        Project project = projectDAO.create(name);
        assertEquals(name, project.getName());
        assertEquals(name, project.getDescription());

        project = projectDAO.create(name, description);
        assertEquals(name, project.getName());
        assertEquals(description, project.getDescription());
    }

    @Test
    public void shouldClear() {
        Project project = projectDAO.create(name);
        assertTrue(!projectDAO.findAll().isEmpty());

        projectDAO.clear();
        assertTrue(projectDAO.findAll().isEmpty());
    }

    @Test
    public void shouldFindAll() {
        List<Project> projects = new ArrayList<>();

        projects.add(projectDAO.create(name));
        assertTrue(projects.equals(projectDAO.findAll()));

        projects.add(projectDAO.create(name, description));
        assertTrue(projects.equals(projectDAO.findAll()));
    }

    @Test
    public void shouldFind() {
        Project project = projectDAO.create(name, description);

        assertEquals(projectDAO.findById(project.getId()), project);
        assertEquals(projectDAO.findByName(project.getName()), project);
        assertEquals(projectDAO.findByIndex(0), project);

        assertEquals(projectDAO.findById(1L), null);
        assertEquals(projectDAO.findByName("1"), null);
        assertEquals(projectDAO.findByIndex(1), null);
    }

    @Test
    public void shouldRemove() {
        Project project = projectDAO.create(name, description);
        assertEquals(projectDAO.removeById(project.getId()), project);

        project = projectDAO.create(name, description);
        assertEquals(projectDAO.removeByName(project.getName()), project);

        project = projectDAO.create(name, description);
        assertEquals(projectDAO.removeByIndex(0), project);

        assertEquals(projectDAO.removeById(1L), null);
        assertEquals(projectDAO.removeByName("1"), null);
        assertEquals(projectDAO.removeByIndex(1), null);
    }

    @Test
    public void shouldUpdate() {
        final String updateName = "9";
        final String updateDescription = "10";
        Project project = projectDAO.create(name, description);

        assertEquals(projectDAO.updateByIndex(0, updateName, null), project);
        assertEquals(project.getName(), updateName);

        assertEquals(projectDAO.updateById(project.getId(), updateDescription, updateName), project);
        assertEquals(project.getName(), updateDescription);
        assertEquals(project.getDescription(), updateName);
    }

}