package ru.mirsaitov.tm.dao;

import org.junit.Test;
import ru.mirsaitov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TaskDAOTest {

    private final String name = "name";

    private final String description = "description";

    private final TaskDAO taskDAO = new TaskDAO();

    @Test
    public void shouldCreate() {
        Task task = taskDAO.create(name);
        assertEquals(name, task.getName());
        assertEquals(name, task.getDescription());

        task = taskDAO.create(name, description);
        assertEquals(name, task.getName());
        assertEquals(description, task.getDescription());
    }

    @Test
    public void shoudlClear() {
        Task project = taskDAO.create(name);
        assertTrue(!taskDAO.findAll().isEmpty());

        taskDAO.clear();
        assertTrue(taskDAO.findAll().isEmpty());
    }

    @Test
    public void shouldFindAll() {
        List<Task> projects = new ArrayList<>();

        projects.add(taskDAO.create(name));
        assertTrue(projects.equals(taskDAO.findAll()));

        projects.add(taskDAO.create(name, description));
        assertTrue(projects.equals(taskDAO.findAll()));
    }

    @Test
    public void shouldFind() {
        Task task = taskDAO.create(name, description);

        assertEquals(taskDAO.findById(task.getId()), task);
        assertEquals(taskDAO.findByName(task.getName()), task);
        assertEquals(taskDAO.findByIndex(0), task);

        assertEquals(taskDAO.findById(1L), null);
        assertEquals(taskDAO.findByName("1"), null);
        assertEquals(taskDAO.findByIndex(1), null);
    }

    @Test
    public void shouldRemove() {
        Task task = taskDAO.create(name, description);
        assertEquals(taskDAO.removeById(task.getId()), task);

        task = taskDAO.create(name, description);
        assertEquals(taskDAO.removeByName(task.getName()), task);

        task = taskDAO.create(name, description);
        assertEquals(taskDAO.removeByIndex(0), task);

        assertEquals(taskDAO.removeById(1L), null);
        assertEquals(taskDAO.removeByName("1"), null);
        assertEquals(taskDAO.removeByIndex(1), null);
    }

    @Test
    public void shouldUpdate() {
        final String updateName = "9";
        final String updateDescription = "10";
        Task task = taskDAO.create(name, description);

        assertEquals(taskDAO.updateByIndex(0, updateName, null), task);
        assertEquals(task.getName(), updateName);

        assertEquals(taskDAO.updateById(task.getId(), updateDescription, updateName), task);
        assertEquals(task.getName(), updateDescription);
        assertEquals(task.getDescription(), updateName);
    }

}