package ru.mirsaitov.tm.dao;

import ru.mirsaitov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskDAO {

    private final List<Task> tasks = new ArrayList<>();

    /**
     * Create task by name
     *
     * @param name - name of task
     * @return created task
     */
    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        return task;
    }

    /**
     * Create task by name and description
     *
     * @param name        - name of task
     * @param description - description of task
     * @return created task
     */
    public Task create(final String name, final String description) {
        final Task task = new Task(name, description);
        tasks.add(task);
        return task;
    }

    /**
     * Clear tasks
     */
    public void clear() {
        tasks.clear();
    }

    /**
     * Find task by index
     *
     * @param index index of task
     * @return task or null
     */
    public Task findByIndex(final int index) {
        if (index < 0 || index >= tasks.size()) {
            return null;
        }
        return tasks.get(index);
    }

    /**
     * Find task by name
     *
     * @param name name of task
     * @return task or null
     */
    public Task findByName(final String name) {
        if (name == null) {
            return null;
        }
        for (final Task task : tasks) {
            if (name.equals(task.getName())) {
                return task;
            }
        }
        return null;
    }

    /**
     * Find task by id
     *
     * @param id id of task
     * @return task or null
     */
    public Task findById(final Long id) {
        if (id == null) {
            return null;
        }
        for (final Task task : tasks) {
            if (id.equals(task.getId())) {
                return task;
            }
        }
        return null;
    }

    /**
     * Remove task by index
     *
     * @param index index of task
     * @return task or null
     */
    public Task removeByIndex(final int index) {
        Task task = findByIndex(index);
        if (task != null) {
            tasks.remove(task);
        }
        return task;
    }

    /**
     * Remove task by name
     *
     * @param name name of task
     * @return task or null
     */
    public Task removeByName(final String name) {
        Task task = findByName(name);
        if (task != null) {
            tasks.remove(task);
        }
        return task;
    }

    /**
     * Remove task by id
     *
     * @param id id of task
     * @return task or null
     */
    public Task removeById(final Long id) {
        Task task = findById(id);
        if (task != null) {
            tasks.remove(task);
        }
        return task;
    }

    /**
     * Update task by index
     *
     * @param index index of task
     * @param name name of task
     * @param description description of task
     * @return task or null
     */
    public Task updateByIndex(final int index, final String name, final String description) {
        Task task = findByIndex(index);
        if (task == null) {
            return null;
        }
        task.setName(name);
        if (description != null) {
            task.setDescription(description);
        }
        return task;
    }

    /**
     * Update task by id
     *
     * @param id id of project
     * @param name name of project
     * @param description description of project
     * @return project or null
     */
    public Task updateById(final Long id, final String name, final String description) {
        Task task = findById(id);
        if (task == null) {
            return null;
        }
        task.setName(name);
        if (description != null) {
            task.setDescription(description);
        }
        return task;
    }

    /**
     * Return tasks
     */
    public List<Task> findAll() {
        return new ArrayList<>(tasks);
    }

}